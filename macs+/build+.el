;;; build+.el -*- lexical-binding: t; -*-

(use-package imake
  :commands imake)

(provide 'build+)
;;; build+.el ends here
