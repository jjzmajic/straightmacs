;;; kbd+.el -*- lexical-binding: t; -*-

;;; vars
(defvar kbd+leader "SPC")
(defvar kbd+localleader ",")
(defvar kbd+alt-leader "M-SPC")
(defvar kbd+alt-localleader "M-,")

;;; funs
(defun kbd+scratch nil
  "Switch to the `*scratch*' buffer."
  (interactive)
  (let ((scratch-buffer "*scratch*"))
    (when (not (get-buffer scratch-buffer))
      (get-buffer-create scratch-buffer))
    (switch-to-buffer-other-window scratch-buffer)))

(defun kbd+messages nil
  "Switch to the `*Messages*' buffer."
  (interactive)
  (switch-to-buffer (messages-buffer)))

;;; packages
(use-package general
  :config
  (defalias 'with-eval-after-load
    #'general-with-eval-after-load)
  (general-create-definer kbd+
    :keymaps 'override
    :states (cdr general-describe-evil-states)
    :prefix kbd+leader
    :non-normal-prefix kbd+alt-leader)
  (general-create-definer kbd+local
    ;; no keymap defaults
    :states (cdr general-describe-evil-states)
    :prefix kbd+localleader
    :non-normal-prefix kbd+alt-localleader))

(use-package helm-descbinds
  :after helm+
  :config (helm-descbinds-mode)
  :general (kbd+ "hb"
             '(helm-descbinds
               :wk "describe bindings")))

;;; keys
(kbd+
  "u" '(universal-argument
        :wk "universal argument")
  "b" '(:wk "buffer")
  "c" '(:wk "code")
  "g" '(:wk "git/gif")
  "f" '(:wk "file")
  "h" '(:wk "help")
  "i" '(:wk "insert")
  "j" '(:wk "jump")
  "o" '(:wk "open/org")
  "p" '(:wk "project")
  "q" '(:wk "quit")
  "r" '(:wk "reload")
  "s" '(:wk "search")
  "t" '(:wk "toggle")
  "w" '(:wk "window")
  ;; +++
  )

(kbd+
  :infix "q"
  "q" '(save-buffers-kill-emacs :wk "quit emacs")
  "k" '(kill-emacs :wk "kill emacs"))

(kbd+
  :infix "t"
  "d" '(toggle-debug-on-error :wk "debug error")
  "q" '(toggle-debug-on-quit :wk "debug quit"))

(kbd+
  :infix "h"
  "c" '(describe-char :wk "character")
  "t" '(describe-theme :wk "theme")
  "p" '(describe-package :wk "package"))

(kbd+
  :infix "f"
  "v" '(:wk "variables")
  "s" '(save-buffer :wk "save buffer"))

(kbd+
  :infix "fv"
  "f" '(add-file-local-variable :wk "file local")
  "d" '(add-dir-local-variable :wk "dir local"))

(kbd+
  :infix "b"
  "d" '(kill-current-buffer :wk "delete")
  "s" '(kbd+scratch :wk "scratch")
  "m" '(kbd+messages :wk "messages")
  "p" '(previous-buffer :wk "previous")
  "n" '(next-buffer :wk "next"))

(provide 'kbd+)
;;; kbd+.el ends here
