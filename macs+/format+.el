;;; format+.el -*- lexical-binding: t; -*-

(after+ 'emacs+
  (crux-with-region-or-buffer indent-region))

(provide 'format+)
;;; format+.el ends here
