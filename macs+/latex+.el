;;; latex+.el -*- lexical-binding: t; -*-

;;; vars
(defvar latex+
  (cond
   ((executable-find "lualatex") 'luatex)
   ((executable-find "xelatex") 'xetex)
   (t 'default)))

;;; funs
(defun latex+compile (&rest _)
  (interactive)
  (let ((TeX-save-query nil))
    (TeX-save-document (TeX-master-file)))
  (TeX-command "LatexMk" #'TeX-master-file -1))

;;; packages
(use-package tex
  :straight auctex
  :init
  (setq TeX-PDF-mode nil
        TeX-engine latex+)
  :gfhook
  ('LaTeX-mode-hook
   '(TeX-fold-mode
     flycheck-mode
     reftex-mode
     auto-fill-mode))
  :init
  (after+ 'flycheck
    (flycheck-add-next-checker 'tex-chktex 'textlint)
    (list+ 'flycheck-textlint-plugin-alist
           '(LaTeX-mode . "latex")))
  :general
  (kbd+local
    :keymaps 'LaTeX-mode-map
    "," #'TeX-command-master
    "b" #'latex-insert-block
    "i" #'latex-insert-item
    "c" '(helm-bibtex-with-local-bibliography
          :wk "cite" :package helm-bibtex)
    "r" '(helm-bibtex
          :package helm-bibtex
          :wk "reference")))


(use-package auctex-latexmk
  :after tex
  :init (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  :config (auctex-latexmk-setup))

(use-package company-auctex
  :after tex company)
(use-package company-math
  :after tex company)

;;; calls
(after+ 'handle
  (handle '(tex-mode latex-mode)
          :docs #'TeX-doc
          :compilers #'latex+compile
          :formatters #'LaTeX-fill-buffer
          :gotos #'dumb-jump-go))

(after+ 'compdef
  (compdef
   :modes #'LaTeX-mode
   :company '((company-reftex-citations
               company-reftex-labels)
              (company-auctex-macros
               company-auctex-symbols
               company-auctex-environments)
              ;; (company-auctex-labels
              ;;  company-auctex-bibs)
              (company-math-symbols-latex
               company-math-symbols-unicode
               company-latex-commands)
              ;; misc
              company-capf)))

(provide 'latex+)
;;; latex+.el ends here
