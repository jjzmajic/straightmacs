;;; python+.el -*- lexical-binding: t; -*-

(defvar python+maps '(python-mode-map inferior-python-mode-map))
(defvar python+executable "jupyter"
  "Executable providing python repl.")

(defun python+jupyter nil
  (setq python-shell-interpreter python+executable
        python-shell-interpreter-args "console --simple-prompt"
        python-shell-interpreter-interactive-arg ""
        python-shell-prompt-detect-failure-warning nil
        python-shell-completion-native-enable nil))

(use-package python-x
  :commands (python-x-setup)
  :hook (python-mode . python-x-setup)
  :general
  (general-def
    :keymaps 'python-mode-map
    "M-p" #'python-backward-fold-or-section
    "M-n" #'python-forward-fold-or-section
    [M-return] #'python-shell-send-fold-or-section-and-step)
  (kbd+local
    :keymaps python+maps
    kbd+localleader #'python-shell-send-paragraph-and-step
    "s" '(:ignore t :wk "shell"))
  (kbd+local
    :infix "s"
    :keymaps python+maps
    "r" #'python-shell-send-region
    "l" #'python-shell-send-line
    "d" #'python-shell-send-defun))

(use-package blacken
  :defer t)

;; calls
(after+ 'python
  (modify-syntax-entry
   ?' "$'" inferior-python-mode-syntax-table)
  (when (executable-find python+executable)
    (python+jupyter)))

(after+ 'handle
  (handle '(python-mode inferior-python-mode)
          :repls #'run-python
          :formatters #'blacken-buffer
	  :evaluators #'python-shell-send-buffer
          :docs '(lsp-describe-thing-at-point
                  python-help-for-region-or-symbol
                  python-eldoc-for-region-or-symbol
                  python-shell-print-region-or-symbol)))

(after+ 'lsp+
  (hook+ 'python-mode-hook
         #'lsp-deferred))

(after+ 'compdef
  (unless (featurep 'lsp+)
    (compdef
     :modes '(python-mode inferior-python-mode)
     :capf '(python-completion-at-point
             comint-completion-at-point
             t))))

(provide 'python+)
;;; python+.el ends here
