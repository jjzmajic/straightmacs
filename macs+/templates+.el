;;; templates+.el -*- lexical-binding: t; -*-

(use-package yatemplate
  :ghook ('prog-mode-hook #'yatemplate-fill-alist)
  :init (setq yatemplate-dir (dirs+templates+))
  :config (auto-insert-mode +1))

(provide 'templates+)
;;; templates+.el ends here
